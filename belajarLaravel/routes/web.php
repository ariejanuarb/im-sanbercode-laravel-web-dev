<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use PHPUnit\Framework\RiskyTestError;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//endpoint/route
Route::get('/', [HomeController::class, 'dashboard']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'kirim']);

Route::get('/table', function() {
    return view('page.table');
});

Route::get('/data-tables', function() {
    return view('page.data-table');
});

// membuat template
// Route::get('/master', function(){
// return view('layout.master');
// });


// CURD Cast

// Create Data
// 1. route untuk mengarah ke halaman form tambah data
route::get('/cast/create', [CastController::class, 'create']);
// 2. route untuk menyimpan data ke database
route::post('/cast', [CastController::class, 'store']);

// Read Data
// 1. route untuk tampil semua cast
route::get('/cast', [CastController::class, 'index']);
// 2. route detail data cast
route::get('/cast/{id}', [CastController::class, 'show']);


// Update Data
// 1. route untuk mengarah ke form update dengan membawa data id
route::get('/cast/{id}/edit', [CastController::class,'edit']);
// 2. route untuk update data berdasarkan id cast
route::put('/cast/{id}', [CastController::class,'update']);


// Deleta data
// 1. route untuk hapus data berdasarkan id cast
route::delete('/cast/{id}', [CastController::class, 'destroy']);