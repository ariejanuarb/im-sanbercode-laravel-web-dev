@extends('layout.master')

@section('title')
Tampil Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Name</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
            <tr>
                <th scope="row">{{$key+1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>

                <td>
                    {{-- /cast/{id}/edit --}}
                    
                    <form action="/cast/{{$value->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        
                    </form>
                </td>
            </tr>
            
        @empty
            <tr>
                <td>Data Cast Kosong</td>
            </tr>
        @endforelse
     
    </tbody>
  </table>
@endsection