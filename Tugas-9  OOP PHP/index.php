<?php

// RELEASE 0 : Buatlah class Animal tersebut di dalam file animal.php.Lakukan instance terhadap class Animal tersebut di file index.php.

require('animal.php'); // ambil fungsi2 yg ada di animal.php
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun"); // tampung object baru dari class Animal, menerima 1 parameter 

echo "Nama Hewan: ". $sheep->name . "<br>";
echo "Jumlah Kaki: ". $sheep->legs . "<br>";
echo "Berdarah Dingin: ". $sheep->cold_blooded. "<br> <br>";


// RELEASE 1 : buatlah class frog dan ape yang merupakan inheritance dari animal...

$kodok = new Frog("buduk");

echo "Name : ". $kodok->name . "<br>";
echo "Legs : ". $kodok->legs . "<br>";
echo "Cold Blooded : ". $kodok->cold_blooded. "<br>";
echo "Jump : "; $kodok->jump();
echo "<br> <br>";

$sungokong = new Ape("kera sakti");

echo "Name : ". $sungokong->name . "<br> ";
echo "Legs : ". $sungokong->legs . "<br>";
echo "Cold Blooded : ". $sungokong->cold_blooded. "<br>";
echo "Yell : "; $sungokong->yell(). "<br>";